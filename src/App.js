import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router} from "react-router-dom";
import RouterApp from "./Router-app";
import React, {Suspense} from 'react';

function App() {
    return (
        <Suspense fallback="loading">
            <Router>
                <RouterApp/>
            </Router>
        </Suspense>
    );
}

export default App;
