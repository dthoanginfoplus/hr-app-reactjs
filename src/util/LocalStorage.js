const defaultExpirationTime = 1440;
export default class LocalStorage extends Map {
    set(key, value, expirationInMin) {
        let expirationDate = new Date(new Date().getTime() + (60000 * expirationInMin || defaultExpirationTime));
        let newValue = {
            value: value,
            expirationDate: expirationDate.toISOString()
        }
        localStorage.setItem(key, JSON.stringify(newValue));
    }

    get(key) {
        let stringValue = localStorage.getItem(key)
        if (stringValue !== null) {
            let value = JSON.parse(stringValue)
            let expirationDate = new Date(value.expirationDate)
            if (expirationDate > new Date()) {
                return value.value
            } else {
                localStorage.removeItem(key)
            }
        }
        return null
    }

    delete(key) {
        localStorage.removeItem(key);
    }
}