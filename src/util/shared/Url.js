const AuthUrl = {
    register: '/auth/register',
    login: '/auth/login',
    checkExistUserID: '/users/auth/checkExistUserID'
}

export {AuthUrl};
