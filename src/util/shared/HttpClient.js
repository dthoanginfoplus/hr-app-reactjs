import axios from 'axios';
const config = {
    headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Accept': '*/*',
    },
    timeout: 60000
}

function postWithoutAuth(Url, body) {
    let result;
    return new Promise(async (resolve, reject) => {
        try {
            result = await axios.post(Url, body, config);
            resolve(result.data);
        } catch (e) {
            reject(e);
        }
    });
}

async function getWithoutAuth(Url) {
    let result;
    return new Promise(async (resolve, reject) => {
        try {
            result = await axios.get(Url, config);
            resolve(result.data);
        } catch (e) {
            reject(e);
        }
    });
}


export {postWithoutAuth, getWithoutAuth};