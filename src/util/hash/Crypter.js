import * as forge from 'node-forge';

const ALGORITHM = 'AES-CBC';
const VECTOR = [0, 1, 2, 3, 4, 5, 6, 7, 7, 6, 5, 4, 3, 2, 1, 0];
const SECRET_KEY = 'SummerDaySadness';

async function encrypt(text) {
    const cipher = forge.cipher.createCipher(ALGORITHM, SECRET_KEY);
    cipher.start({iv: VECTOR});
    cipher.update(forge.util.createBuffer(text));
    cipher.finish()
    const encrypted = cipher.output;
    return forge.util.encode64(encrypted.data);
}

export {encrypt};