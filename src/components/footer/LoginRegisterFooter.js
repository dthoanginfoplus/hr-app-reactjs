import React from "react";
import {Card} from "react-bootstrap";

export default function LoginRegisterFooter() {
    return (
        <div className={"text-center fixed-bottom"}>
            <Card>
                <Card.Body>
                    <blockquote className="blockquote mb-0">
                        <footer className="blockquote-footer">
                            Company <cite title="Source Title">&copy; 2019-2021</cite>
                        </footer>
                    </blockquote>
                </Card.Body>
            </Card>
        </div>
    );
}