import React, {useEffect, useState} from "react";
import {Container, Navbar, NavDropdown} from "react-bootstrap";
import {useTranslation} from "react-i18next";

export default function LoginRegisterHeader() {
    const {i18n} = useTranslation();
    const usaFlag = "https://cdn.britannica.com/33/4833-004-828A9A84/Flag-United-States-of-America.jpg";
    const vnFlag = "https://seeklogo.com/images/V/viet-nam-logo-3D78D597F9-seeklogo.com.png";
    const [language, setLanguage] = useState("");
    const [flag, setFlag] = useState("");
    const [isVnLang, setIsVnLang] = useState(false);

    function getLanguage() {
        if (i18n.language === 'vn') {
            setIsVnLang(true);
            setFlag(vnFlag);
            setLanguage("VN")
        }
        if (i18n.language === 'en') {
            setIsVnLang(false);
            setFlag(usaFlag);
            setLanguage("EN")
        }
    }

    async function changeLange(lang) {
        await i18n.changeLanguage(lang);
        if (lang === "vn")
            setIsVnLang(true);
            setFlag(vnFlag);
            setLanguage("VN")
        if (lang === "en")
            setIsVnLang(false);
            setFlag(usaFlag);
            setLanguage("EN")
        // if (window.location.pathname === '/register')
        //     window.location.href = "/register"
    }

    useEffect(() => {
        getLanguage();
    });
    return (
        <>
            <div className={"fixed-top"}>
                <Navbar bg="light" variant="light">
                    <Container>
                        <Navbar.Brand href="#home">
                            <img
                                alt=""
                                src="https://react-bootstrap.netlify.app/logo.svg"
                                width="30"
                                height="25"
                                className="d-inline-block align-top"
                            />{' '}
                            Reactjs
                        </Navbar.Brand>
                        <Navbar.Collapse id="basic-navbar-nav"
                                         className={"navbar-collapse collapse justify-content-end"}>
                            <img
                                alt=""
                                src={flag}
                                width="35"
                                height="20"
                                className="d-inline-block align-top"
                            />
                            <NavDropdown alignRight title={language} id="basic-nav-dropdown" onSelect={changeLange}>
                                {isVnLang && <NavDropdown.Item eventKey={"en"} className={"text-center"}>
                                    <img
                                        alt=""
                                        src={usaFlag}
                                        width="35"
                                        height="20"
                                        className="d-inline-block align-top"
                                    />
                                    &nbsp;
                                    EN
                                </NavDropdown.Item>}
                                {!isVnLang && <NavDropdown.Item eventKey={"vn"} className={"text-center"}>
                                    <img
                                        alt=""
                                        src={vnFlag}
                                        width="35"
                                        height="20"
                                        className="d-inline-block align-top"
                                    />
                                    &nbsp;
                                    VN
                                </NavDropdown.Item>}
                                {/*<NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>*/}
                                {/*<NavDropdown.Divider/>*/}
                                {/*<NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>*/}
                            </NavDropdown>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
            </div>
        </>
    );
}