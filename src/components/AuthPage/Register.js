import React, {useRef, useState} from "react";
import {useForm} from "react-hook-form";
import * as Icon from "react-bootstrap-icons";
import "../../styles/AuthPageStyle/register.css";
import {Button, Spinner} from "react-bootstrap";
import {useTranslation} from "react-i18next";
import {doRegister, checkExistUserId} from "../../services/AuthService";
import StaticBackdropModalRegister from "../../modal/StaticBackdropModalRegister";
export default function Register(props) {
    const {t} = useTranslation();
    const {register, handleSubmit, formState: {errors}, watch} = useForm(
        {
            mode: 'onChange',
            reValidateMode: 'onChange',
            defaultValues: {},
            validationSchema: undefined, // Note: will be deprecated in the next major version with validationResolver
            validationResolver: undefined,
            validationContext: undefined,
            validateCriteriaMode: "firstErrorDetected",
            submitFocusError: true,
            nativeValidation: false, // Note: version 3 only
        }
    );
    const password = useRef();
    password.current = watch("password", "");
    const [isLoading, setIsLoading] = useState(false);
    const [isExistUser, setExistUser] = useState(false);
    const [isError, setIsError] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    async function onSubmit(data) {
        try {
            setIsError(false);
            if (isExistUser)
                return;
            setIsLoading(true);
            let user = await doRegister(data);
            console.log(user);
            setIsLoading(false);
            document.getElementById("buttonShowStaticBackdropModalRegister").click();
        } catch (e) {
            setIsLoading(false);
            setIsError(true);
            setErrorMessage(e.toString());
            console.log(e);
        }
    }

    async function onChangeUserID(event) {
        setExistUser(false);
        let userID = event.target.value;
        console.log(userID);
        if (userID && userID.length > 5 && userID.length < 20) {
            let resultCheck = await checkExistUserId(userID);
            setExistUser(resultCheck);
        } else {
            setExistUser(false);
        }
    }
    return (
        <>
        <div className={"text-center login"}>
            <StaticBackdropModalRegister title={t("register_page.register_success_title")}
                                         body={t("register_page.register_success_message")}
            />
            <form className="form-signin" onSubmit={handleSubmit(onSubmit)}>
                <img className="mb-4" src="https://react-bootstrap.netlify.app/logo.svg" alt="" width="72" height="72"/>
                <h1 className="h3 mb-3 font-weight-normal">{t("register_page.register")}</h1>
                {/*id*/}
                <input type="text" className="form-control mt-1 mb-1" placeholder={t("register_page.id")}
                       autoFocus="" {...register("userId",
                    {
                        required: {value: true, message: ``},
                        minLength: {value: 6, message: ``},
                        maxLength: {value: 20, message: ``}
                    })} onBlur={onChangeUserID} onInput={() => {
                    setExistUser(false);
                }}/>
                {errors.userId && errors.userId.type === "required" &&
                <em className={"text-danger"}><b>{t("register_page.validate_id")}</b></em>}
                {errors.userId && errors.userId.type === "minLength" &&
                <em className={"text-danger"}><b>{t("register_page.validate_id_min")}</b></em>}
                {errors.userId && errors.userId.type === "maxLength" &&
                <em className={"text-danger"}><b>{t("register_page.validate_id_max")}</b></em>}
                {isExistUser && !errors.userId &&
                <em className={"text-danger"}><b>{t("register_page.validate_userId_already_taken")}</b></em>}
                {/* Name*/}
                <input type="text" className="form-control mt-1 mb-1" placeholder={t("register_page.name")}
                       required=""  {...register("fullName", {
                    required: {value: true, message: ``},
                    maxLength: {value: 200, message: ``}
                })}/>
                {errors.fullName && errors.fullName.type === "required" &&
                <em className={"text-danger"}><b>{t("register_page.validate_name")}</b></em>}
                {errors.fullName && errors.fullName.type === "maxLength" &&
                <em className={"text-danger"}><b>{t("register_page.validate_name_max")}</b></em>}
                {/* PhoneNumber*/}
                <input type="text" className="form-control mt-1 mb-1"
                       placeholder={t("register_page.mobile_phone_number")}
                       required=""  {...register("phoneNumber", {
                    required: false,
                    // eslint-disable-next-line
                    pattern: /(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/
                })}/>
                {errors.phoneNumber && <em className={"text-danger"}><b>{t("register_page.validate_phone")}</b></em>}
                {/*email*/}
                <input type="text" className="form-control mt-1 mb-1" placeholder={t("register_page.email")}
                       autoFocus="" {...register("email",
                    {
                        required: {
                            value: true,
                            message: ``
                        },
                        pattern: {
                            // eslint-disable-next-line
                            value: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                            message: ``
                        }
                    })}/>
                {errors.email && errors.email.type === "required" &&
                <em className={"text-danger"}><b>{t("register_page.validate_email")}</b></em>}
                {errors.email && errors.email.type === "pattern" &&
                <em className={"text-danger"}><b>{t("register_page.validate_email_invalid")}</b></em>}
                {/*password*/}
                <input type="password" className="form-control mt-1 mb-1" placeholder={t("register_page.password")}
                       required=""  {...register("password", {
                    required: {value: true, message: ``},
                    minLength: {value: 6, message: ``},
                    maxLength: {value: 20, message: ``}
                })}/>
                {errors.password && errors.password.type === "required" &&
                <em className={"text-danger"}><b>{t("register_page.validate_password")}</b></em>}
                {errors.password && errors.password.type === "minLength" &&
                <em className={"text-danger"}><b>{t("register_page.validate_password_min")}</b></em>}
                {errors.password && errors.password.type === "maxLength" &&
                <em className={"text-danger"}><b>{t("register_page.validate_password_max")}</b></em>}
                {/*password*/}
                <input type="password" className="form-control mt-1 mb-1"
                       placeholder={t("register_page.confirm_password")}
                       required=""  {...register("confirmPassword",
                    {
                        validate: value =>
                            value === password.current || ``
                        , required: {value: true, message: ``},
                        minLength: {value: 6, message: ``},
                        maxLength: {value: 20, message: ``}
                    })}/>
                {errors.confirmPassword && errors.confirmPassword.type === "required" &&
                <em className={"text-danger"}><b>{t("register_page.validate_confirm_password")}</b></em>}
                {errors.confirmPassword && errors.confirmPassword.type === "validate" &&
                <em className={"text-danger"}><b>{t("register_page.validate_confirm_password_compare")}</b></em>}
                {errors.confirmPassword && errors.confirmPassword.type === "minLength" &&
                <em className={"text-danger"}><b>{t("register_page.validate_confirm_password_min")}</b></em>}
                {errors.confirmPassword && errors.confirmPassword.type === "maxLength" &&
                <em className={"text-danger"}><b>{t("register_page.validate_confirm_password_max")}</b></em>}
                <div className="checkbox mt-3 mb-3">
                    {isError && <p><em className={"text-danger"}><b>{errorMessage}</b></em></p>}
                </div>
                {!isLoading && <button className={"btn btn-lg btn-outline-primary btn-block mb-5"}
                                       type="submit">{t("register_page.sign_up")}</button>}
                {isLoading &&
                <Button className={"btn btn-lg btn-outline-primary btn-block mb-5"} variant="primary" disabled>
                    <i className={"text-lg-center"}> {t("register_page.loading")}
                    </i>
                    <Spinner
                        as="span"
                        animation="grow"
                        size="lg"
                        role="status"
                        aria-hidden="true"
                    />
                </Button>}
                <i className={"text-info"}>{t("register_page.already_have_an_account")}?&nbsp;
                </i><a href={"/login"}> {t("register_page.sign_in")} <Icon.ArrowRight className={"bi-link"}/></a>
            </form>
        </div>
        </>
    );
}