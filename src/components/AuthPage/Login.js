import React, {useEffect, useState} from "react";
import "../../styles/AuthPageStyle/login.css";
import {useForm} from "react-hook-form";
import * as Icon from 'react-bootstrap-icons';
import {Button, Spinner} from "react-bootstrap";
import {useTranslation} from "react-i18next";
import {doLogin} from '../../services/AuthService';
export default function Login(props) {
    const {register, handleSubmit, formState: {errors}} = useForm(
        {
            mode: 'onChange',
            reValidateMode: 'onChange',
            defaultValues: {},
            validationSchema: undefined, // Note: will be deprecated in the next major version with validationResolver
            validationResolver: undefined,
            validationContext: undefined,
            validateCriteriaMode: "firstErrorDetected",
            submitFocusError: true,
            nativeValidation: false, // Note: version 3 only
        }
    );
    const [isInformationErrorLogin, setIsInformationErrorLogin] = useState(false);
    const [isOtherErrorLogin, setIsOtherErrorLogin] = useState(false);
    const [otherErrorMessage, setOtherErrorMessage] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const {t} = useTranslation();
    async function onSubmit(data) {
        try {
            setIsOtherErrorLogin(false);
            setIsInformationErrorLogin(false);
            setIsLoading(true);
            await doLogin(data);
            setIsLoading(false);
        } catch (e) {
            let message = e.toString();
            if (e.response && e.response.status === 401) {
                setIsInformationErrorLogin(true);
            } else {
                setIsOtherErrorLogin(true);
                setOtherErrorMessage(message);
            }
            setIsLoading(false);
            console.log(e.toString());
        }
    }

    useEffect(() => {
    });

    return (
        <div className={"text-center login"}>
            <form className="form-signin" onSubmit={handleSubmit(onSubmit)}>
                <img className="mb-4" src="https://react-bootstrap.netlify.app/logo.svg" alt="" width="72" height="72"/>
                <h1 className="h3 mb-3 font-weight-normal">{t("login_page.login")}</h1>
                <input maxLength={30} type="text" id="ID" className="form-control mb-1" placeholder={t("login_page.id")}
                       autoFocus="" {...register("userId", {required: true})}/>
                {errors.userId && <em className={"text-danger"}><b>{t("login_page.validate_id")}</b></em>}
                <input maxLength={30}  type="password" id="inputPassword" className="form-control mb-1" placeholder={t("login_page.password")}
                       required=""  {...register("password", {required: true})}/>
                {errors.password && <em className={"text-danger"}><b>{t("login_page.validate_password")}</b></em>}
                <div className="mt-3">
                    <label>
                        <input className={"form-check-input position-static"}
                               type="checkbox" {...register("rememberMe", {required: false})}/><i> {t("login_page.remember_me")}</i>
                    </label>
                </div>
                <div className="mb-3">
                    {isInformationErrorLogin && <em className={"text-danger"}><b>{t("login_page.userid_password_incorrect")}</b></em>}
                    {isOtherErrorLogin && <em className={"text-danger"}><b>{otherErrorMessage}</b></em>}
                </div>
                {!isLoading &&
                <button className={"btn btn-lg btn-outline-primary btn-block mb-5"} type="submit">{t("login_page.sign_in")}</button>}
                {isLoading &&
                <Button className={"btn btn-lg btn-outline-primary btn-block mb-5"} variant="primary" disabled>
                    <i className={"text-lg-center"}> {t("login_page.loading")} &nbsp;
                    </i>
                    <Spinner
                        as="span"
                        animation="grow"
                        size="lg"
                        role="status"
                        aria-hidden="true"
                    />
                </Button>}
                <p className={"text-link"} ><a href={"/register"}>{t("login_page.forgot_password")}?&nbsp;</a></p>
                <i className={"text-info"}>{t("login_page.dont_have_account")}?&nbsp;</i>
                <a href={"/register"}> {t("login_page.sign_up")} <Icon.ArrowRight className={"bi-link"}/></a>
            </form>
        </div>
    );
}