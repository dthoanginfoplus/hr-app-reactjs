import {Role} from './Role';
function UserModel(data) {
    this.userId = data.userId;
    this.password = data.password;
    this.fullName = data.fullName;
    this.phoneNumber = data.phoneNumber;
    this.email = data.email;
    this.status = data.status || 1;
    this.role = new Role(data.role || {roleId: 22, roleName: ''});
}

export {UserModel};