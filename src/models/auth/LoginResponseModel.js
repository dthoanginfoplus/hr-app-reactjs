function LoginResponseModel(data) {
   this.loginTime = data.loginTime;
   this.message = data.message;
   this.token = data.token;
   this.userId = data.userId;
   this.roleName = data.roleName;
}

export {LoginResponseModel};