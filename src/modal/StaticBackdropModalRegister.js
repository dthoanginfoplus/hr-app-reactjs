import React, {useState} from 'react';
import {Button, Modal} from "react-bootstrap";
import { Redirect } from 'react-router-dom';


export default function StaticBackdropModalRegister(props) {

    const [show, setShow] = useState(false);
    const [isRedirect, setIsRedirect] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    if(isRedirect) {
        return (<Redirect to={{
            pathname: "/login",
        }} />)
    }
    return(
        <>
            <Button hidden={true} variant="primary" id={"buttonShowStaticBackdropModalRegister"} onClick={handleShow}>
                Launch static backdrop modal
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header>
                    <Modal.Title>{props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {props.body}
                </Modal.Body>
                <Modal.Footer>
                    <Button hidden={true} variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={() => {setIsRedirect(true)}}>OK</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}