import {postWithoutAuth, getWithoutAuth} from '../util/shared/HttpClient'
import {UserModel} from "../models/User/userModel";
import {LoginModelRequest} from "../models/auth/LoginModelRequest"
import {LoginResponseModel} from "../models/auth/LoginResponseModel"
import {AuthUrl} from "../util/shared/Url";
import LocalStorage from "../util/LocalStorage";
import KeyLocalStorage from "../util/KeyLocalStorage";
import {encrypt} from '../util/hash/Crypter';

const storageLocal = new LocalStorage()

function doRegister(data) {
    return new Promise(async (resolve, reject) => {
        try {
            let dataRegister = new UserModel(data);
            dataRegister.password = await encrypt(dataRegister.password);
            console.log('dataRegister ', dataRegister);
            let resultRegister = await postWithoutAuth(AuthUrl.register, dataRegister);
            let user = new UserModel(resultRegister);
            resolve(user)
        } catch (e) {
            reject(e);
        }
    });
}

function doLogin(data) {
    return new Promise(async (resolve, reject) => {
        try {
            let dataLogin = new LoginModelRequest(data);
            dataLogin.password = await encrypt(dataLogin.password);
            console.log(dataLogin);
            let responseLogin = await postWithoutAuth(AuthUrl.login, dataLogin)
            let resultLogin = new LoginResponseModel(responseLogin);
            storageLocal.set(KeyLocalStorage.USER_INFO_LOGIN, resultLogin);
            console.log('local data', storageLocal.get(KeyLocalStorage.USER_INFO_LOGIN));
            resolve(resultLogin);
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });
}

function checkExistUserId(userId) {
    return new Promise(async (resolve, reject) => {
        try {
            let URL = AuthUrl.checkExistUserID + `?userId=${userId}`;
            let resultCheck = await getWithoutAuth(URL);
            console.log('resultCheck', resultCheck);
            resolve(resultCheck.exist);
        } catch (e) {
            console.log(e.toString());
            reject(e);
        }
    });
}

export {doRegister, checkExistUserId, doLogin};