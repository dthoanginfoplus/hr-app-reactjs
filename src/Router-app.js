import React from "react";
import {Switch, Route} from "react-router-dom";
import Login from "./components/AuthPage/Login";
import LoginRegisterHeader from "./components/header/LoginRegisterHeader";
import LoginRegisterFooter from "./components/footer/LoginRegisterFooter";
import Register from "./components/AuthPage/Register";

export default function RouterApp(props) {
    if (props.isValidToken)
        return (
            <Switch>
                <Route>
                </Route>
            </Switch>
        );
    else
        return (
            <>
                <LoginRegisterHeader/>
                <Switch>
                    <Route path={"/login"} exact>
                        <Login/>
                    </Route>
                    <Route path={"/register"} exact>
                        <Register/>
                    </Route>
                    <Route path={"*"}>
                        <Login/>
                    </Route>
                </Switch>
                <LoginRegisterFooter/>
            </>
        );
}